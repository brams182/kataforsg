package integration;

import java.util.*;

import com.bbiki.sgkatabank.dao.BankAccountRepository;
import com.bbiki.sgkatabank.domain.AccountOperation;
import com.bbiki.sgkatabank.domain.Amount;
import com.bbiki.sgkatabank.domain.exceptions.BankAccountNotFoundException;

public class InMemoryBankAccountRepository implements BankAccountRepository {
    private final Map<String, List<AccountOperation>> data = new HashMap<>();

    public void createAccount(String accountNumber) {
        data.put(accountNumber, new LinkedList<>());
    }

    @Override
    public void addOperation(String accountNumber, AccountOperation accountOperation) throws BankAccountNotFoundException {
        List<AccountOperation> operations = data.get(accountNumber);

        operations.add(accountOperation);
    }

    @Override
    public List<AccountOperation> getAccountOperations(String accountNumber) throws BankAccountNotFoundException {
        return data.get(accountNumber);
    }
}
