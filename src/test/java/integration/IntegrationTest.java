package integration;

import com.bbiki.sgkatabank.dao.BankAccountRepository;
import com.bbiki.sgkatabank.domain.Amount;
import com.bbiki.sgkatabank.domain.exceptions.BankAccountNotFoundException;
import com.bbiki.sgkatabank.domain.exceptions.InsufficientBalanceException;
import com.bbiki.sgkatabank.domain.exceptions.InvalidAmountException;
import com.bbiki.sgkatabank.service.TimeStampService;
import com.bbiki.sgkatabank.service.account.AccountService;
import com.bbiki.sgkatabank.service.printer.TabularStatementPrinterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Instant;

public class IntegrationTest {
    private InMemoryBankAccountRepository bankAccountRepository;
    private TimeStampService timeService;
    private AccountService service;

    @BeforeEach
    public void setUp() {
        bankAccountRepository = new InMemoryBankAccountRepository();
        timeService = () -> Instant.parse("2011-12-03T01:00:00.00Z") ;

        service = new AccountService(bankAccountRepository, timeService);
    }

    @Test
    @DisplayName("Sample integration demonstration")
    public void sampleIntegrationDemonstration() throws BankAccountNotFoundException, InvalidAmountException, InsufficientBalanceException {
        bankAccountRepository.createAccount("100");
        bankAccountRepository.createAccount("200");

        service.deposit("100", Amount.of(10.0));
        service.deposit("100", Amount.of(10.0));
        service.deposit("200", Amount.of(30.0));

        service.withdraw("100", Amount.of(-5.0));
        service.withdraw("100", Amount.of(-5.0));

        String accountStatement100 = service.print("100", new TabularStatementPrinterService());
        String accountStatement200 = service.print("200", new TabularStatementPrinterService());
    }
}
