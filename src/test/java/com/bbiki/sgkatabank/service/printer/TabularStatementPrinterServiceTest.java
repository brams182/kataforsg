
package com.bbiki.sgkatabank.service.printer;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.*;
import java.util.ArrayList;
import java.util.List;

import com.bbiki.sgkatabank.service.StatementPrinterService;
import com.bbiki.sgkatabank.service.printer.TabularStatementPrinterService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.bbiki.sgkatabank.domain.AccountOperation;
import com.bbiki.sgkatabank.domain.Amount;
import com.bbiki.sgkatabank.domain.exceptions.InvalidAmountException;

public class TabularStatementPrinterServiceTest {
    private StatementPrinterService printDataService;
    private List<AccountOperation> accountOperations;
    private static final String EMPTY_LINE = "--------------------------------------------------------------------------------";
    private static final String HEADER = "                Date     |          credit          |           debit          |";
    private static final String LINE_STOP = System.lineSeparator();
    private Instant defaultInstant =  Instant.parse("2011-12-03T01:00:00.00Z") ;

    @BeforeEach
    public void setUp() {
        printDataService = new TabularStatementPrinterService();
        accountOperations = new ArrayList();
    }

    @AfterEach
    public void tearDown() {
        accountOperations.clear();
    }

    @Test
    public void testCaseWhenAccountHasOneOperation()  throws InvalidAmountException{
        StringBuilder expected = new StringBuilder();
        expected.append(HEADER + LINE_STOP);
        expected.append(EMPTY_LINE + LINE_STOP);
        expected.append(
                "2011-12-03T01:00:00Z     |                          |          -10.00          |");
        expected.append(LINE_STOP);
        expected.append("Balance: ");
        expected.append(LINE_STOP);
        expected.append("10.00");
         accountOperations.add(   AccountOperation.createWithdrawalOperation(Amount.of(-10D), defaultInstant));

        assertThat(printDataService.printStatement(accountOperations,
                Amount.of(10))).isEqualTo(expected.toString());
    }

    @Test
    public void testCaseWhenAccountHasNoOperations() {
        StringBuilder expected = new StringBuilder();
        expected.append(HEADER + LINE_STOP);
        expected.append(EMPTY_LINE + LINE_STOP);
        expected.append("Balance: ");
        expected.append(LINE_STOP);
        expected.append("0.00");
        assertThat(printDataService.printStatement(accountOperations,
                Amount.of(0.0))).isEqualTo(expected.toString());
    }

    @Test
    public void testCaseWhenAccountHasManyWithdrawOperations() throws InvalidAmountException {
        StringBuilder expected = new StringBuilder();
        expected.append(HEADER + LINE_STOP);
        expected.append(EMPTY_LINE +
                LINE_STOP);
        expected.append(
                "2011-12-03T01:00:00Z     |                          |          -10.00          |");
        expected.append(LINE_STOP);
        expected.append("2011-12-03T01:00:00Z     |                          |          -10.00          |");
        expected.append(LINE_STOP);
        expected.append("Balance: ");
        expected.append(LINE_STOP);
        expected.append("20.00");
        accountOperations.add( AccountOperation.createWithdrawalOperation(Amount.of(-10D), defaultInstant));
        accountOperations.add( AccountOperation.createWithdrawalOperation(Amount.of(-10D), defaultInstant));
        assertThat(printDataService.printStatement(accountOperations,
                Amount.of(20))).isEqualTo(expected.toString());
    }

    @Test
    public void testCaseWhenAccountHasManyDepositOperations() throws InvalidAmountException {
        StringBuilder expected = new StringBuilder();
        expected.append(HEADER + LINE_STOP);
        expected.append(EMPTY_LINE +
                LINE_STOP);
        expected.append(
                "2011-12-03T01:00:00Z     |           10.00          |                          |"
                        + LINE_STOP);
        expected.append(
                "2011-12-03T01:00:00Z     |           10.00          |                          |"
                        + LINE_STOP);
        expected.append("Balance: ");
        expected.append(LINE_STOP);
        expected.append("20.00");
        accountOperations.add(  AccountOperation.createDepositOperation(Amount.of(10D), defaultInstant));
        accountOperations.add(  AccountOperation.createDepositOperation(Amount.of(10D), defaultInstant));
        assertThat(printDataService.printStatement(accountOperations, Amount.of(20))).isEqualTo(expected.toString());
    }

    @Test
    public void testCaseWhenAccountHasDepositAndWithdrawOperations() throws InvalidAmountException {
        StringBuilder expected = new StringBuilder();
        expected.append(HEADER + LINE_STOP);
        expected.append(EMPTY_LINE +
                LINE_STOP);
        expected.append(
                "2011-12-03T01:00:00Z     |           30.00          |                          |"
                        + LINE_STOP);
        expected.append(
                "2011-12-03T01:00:00Z     |                          |          -10.00          |");
        expected.append(LINE_STOP);
        expected.append("Balance: ");
        expected.append(LINE_STOP);
        expected.append("20.00");
        accountOperations.add(  AccountOperation.createDepositOperation(Amount.of(30D), defaultInstant));
        accountOperations.add(  AccountOperation.createWithdrawalOperation(Amount.of(-10D), defaultInstant));
        assertThat(printDataService.printStatement(accountOperations,
                Amount.of(20D))).isEqualTo(expected.toString());
    }
}
