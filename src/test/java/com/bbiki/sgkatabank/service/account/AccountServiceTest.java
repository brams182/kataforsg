package com.bbiki.sgkatabank.service.account;

import com.bbiki.sgkatabank.dao.BankAccountRepository;
import com.bbiki.sgkatabank.domain.AccountOperation;
import com.bbiki.sgkatabank.domain.Amount;
import com.bbiki.sgkatabank.domain.exceptions.BankAccountNotFoundException;
import com.bbiki.sgkatabank.domain.exceptions.InsufficientBalanceException;
import com.bbiki.sgkatabank.domain.exceptions.InvalidAmountException;
import com.bbiki.sgkatabank.service.TimeStampService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

public class AccountServiceTest {
    private static final Instant SAMPLE_INSTANT = Instant.parse("2011-12-03T01:00:00.00Z");
    public static final String SAMPLE_ACCOUNT_NUMBER = "1337";

    @Mock
    private TimeStampService timeStampServiceMock;

    @Mock
    private BankAccountRepository bankAccountRepositoryMock;

    private AccountService accountService;

    @BeforeEach
    void beforeEach() {
        timeStampServiceMock = mock(TimeStampService.class);
        bankAccountRepositoryMock = mock(BankAccountRepository.class);
        accountService = new AccountService(bankAccountRepositoryMock, timeStampServiceMock);
    }

    @Test
    @DisplayName("Should be able to make a deposit to an existing account")
    void shouldBeAbleToMakeADepositToAnExistingAccount() throws BankAccountNotFoundException, InvalidAmountException {
        final Amount depositAmount = Amount.of(100.0);

        final AccountOperation expectedDepositOperation = AccountOperation.createDepositOperation(depositAmount, SAMPLE_INSTANT);
        when(timeStampServiceMock.utcNow()).thenReturn(SAMPLE_INSTANT);

        accountService.deposit(SAMPLE_ACCOUNT_NUMBER, depositAmount);

        verify(bankAccountRepositoryMock, times(1)).addOperation(SAMPLE_ACCOUNT_NUMBER, expectedDepositOperation);
        verifyNoMoreInteractions(bankAccountRepositoryMock);

        verify(timeStampServiceMock, times(1)).utcNow();
        verifyNoMoreInteractions(timeStampServiceMock);
    }

    @Test
    @DisplayName("Should not be able to make a deposit with an invalid amount")
    void shouldNotBeAbleToMakeADepositWithAnInvalidAmount()   {
        final Amount depositAmount = Amount.of(-100.0);

        Throwable thrown = catchThrowable(() -> accountService.deposit(SAMPLE_ACCOUNT_NUMBER, depositAmount));

        assertThat(thrown).isInstanceOf(InvalidAmountException.class);

        verifyZeroInteractions(bankAccountRepositoryMock);
    }

    @Test
    @DisplayName("Should not be able to make a deposit to non existing account")
    void shouldNotBeAbleToMakeADepositToNonExistingBankAccount() throws BankAccountNotFoundException, InvalidAmountException {
        final Amount amount = Amount.of(100.0);
        final AccountOperation depositOperation = AccountOperation.createDepositOperation(amount, SAMPLE_INSTANT);

        when(timeStampServiceMock.utcNow()).thenReturn(SAMPLE_INSTANT);

        doThrow(new BankAccountNotFoundException(SAMPLE_ACCOUNT_NUMBER))
                .when(bankAccountRepositoryMock)
                .addOperation(SAMPLE_ACCOUNT_NUMBER, depositOperation);

        Throwable thrown = catchThrowable(() -> accountService.deposit(SAMPLE_ACCOUNT_NUMBER, amount));

        assertThat(thrown).isInstanceOf(BankAccountNotFoundException.class);

        verify(bankAccountRepositoryMock, times(1)).addOperation(SAMPLE_ACCOUNT_NUMBER, depositOperation);
        verifyNoMoreInteractions(bankAccountRepositoryMock);

        verify(timeStampServiceMock, times(1)).utcNow();
        verifyNoMoreInteractions(timeStampServiceMock);
    }

    @Test
    @DisplayName("Should be able to make a withdraw to an existing account")
    void shouldBeAbleToMakeAWithdrawToAnExistingAccount() throws BankAccountNotFoundException, InvalidAmountException, InsufficientBalanceException {
        final Amount initialDepositAmount = Amount.of(200.0);
        final List<AccountOperation> expectedListOperations = Arrays.asList(AccountOperation.createDepositOperation(initialDepositAmount, SAMPLE_INSTANT));

        final Amount withdrawalAmount = Amount.of(-100.0);
        final AccountOperation expectedWithdrawalOperation = AccountOperation.createWithdrawalOperation(withdrawalAmount, SAMPLE_INSTANT);

        when(bankAccountRepositoryMock.getAccountOperations(SAMPLE_ACCOUNT_NUMBER)).thenReturn(expectedListOperations);

        when(timeStampServiceMock.utcNow()).thenReturn(SAMPLE_INSTANT);

        accountService.withdraw(SAMPLE_ACCOUNT_NUMBER, withdrawalAmount);

        verify(bankAccountRepositoryMock, times(1)).getAccountOperations(SAMPLE_ACCOUNT_NUMBER);
        verify(bankAccountRepositoryMock, times(1)).addOperation(SAMPLE_ACCOUNT_NUMBER, expectedWithdrawalOperation);
        verifyNoMoreInteractions(bankAccountRepositoryMock);

        verify(timeStampServiceMock, times(1)).utcNow();
        verifyNoMoreInteractions(timeStampServiceMock);
    }

    @Test
    @DisplayName("Should not be able to make a withdraw with an invalid amount")
    void shouldNotBeAbleToMakeAWithdrawWithInvalidAmount()   {
        final Amount amount = Amount.of(100.0);

        Throwable thrown = catchThrowable(() -> accountService.withdraw(SAMPLE_ACCOUNT_NUMBER, amount));

        assertThat(thrown).isInstanceOf(InvalidAmountException.class);

        verifyZeroInteractions(bankAccountRepositoryMock);
    }

    @Test
    @DisplayName("Should not be able to make a withdraw to non existing account")
    void shouldNotBeAbleToMakeAWithdrawToNonExistingBankAccount() throws BankAccountNotFoundException {
        final Amount amount = Amount.of(-100.0);

        when(bankAccountRepositoryMock.getAccountOperations(SAMPLE_ACCOUNT_NUMBER)).thenThrow(new BankAccountNotFoundException(SAMPLE_ACCOUNT_NUMBER));

        Throwable thrown = catchThrowable(() -> accountService.withdraw(SAMPLE_ACCOUNT_NUMBER, amount));

        assertThat(thrown).isInstanceOf(BankAccountNotFoundException.class);

        verify(bankAccountRepositoryMock, times(1)).getAccountOperations(SAMPLE_ACCOUNT_NUMBER);
        verifyNoMoreInteractions(bankAccountRepositoryMock);
    }
}
