package com.bbiki.sgkatabank.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class AmountTest {

    @ParameterizedTest
    @ValueSource(doubles = {
            3D, 5D, 79D, 80D
    })
    void shouldAcceptPositivesDoubleValues(double value) {
        Throwable thrown = catchThrowable(() -> Amount.of(value));
        assertThat(thrown).isNull();
    }

    @ParameterizedTest
    @ValueSource(doubles = {
            -3D, -5D, -79D, -80D
    })
    void shouldAcceptNegativesDoubleValues(double value) {
        Throwable thrown = catchThrowable(() -> Amount.of(value));
        assertThat(thrown).isNull();
    }

    @Test
    void shouldBeGreater() {
        assertThat(Amount.of(10D)).isGreaterThan(Amount.of(5));
    }

    @Test
    void shouldBeLess( ) {
        assertThat(Amount.of(10D)).isLessThan(Amount.of(11));
    }

    @Test
    void shouldBeEquals() {
        assertThat(Amount.of(10D) ).isEqualTo(Amount.of(10.0));
    }
    @Test
    void shouldBeNegative() {
        assertTrue(Amount.of(-10D).isNegative());
    }
    @Test
    void shouldBePositive() {
        assertTrue(Amount.of(10D).isPositive());
    }
}
