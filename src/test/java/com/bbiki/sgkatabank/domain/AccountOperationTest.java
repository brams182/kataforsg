package com.bbiki.sgkatabank.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.time.Instant;

import com.bbiki.sgkatabank.domain.AccountOperation;
import com.bbiki.sgkatabank.domain.OperationType;
import com.bbiki.sgkatabank.domain.exceptions.InvalidAmountException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.bbiki.sgkatabank.domain.Amount;

class AccountOperationTest {
    private final Instant sampleInstant = Instant.parse("2011-12-03T01:00:00.00Z");

    @Test
    @DisplayName("Should be able to create a deposit operation given a positive amount")
    void canCreateADepositWithPositiveAmount() throws InvalidAmountException {
        final Amount depositAmount = Amount.of(40D);

        final AccountOperation deposit = AccountOperation.createDepositOperation(depositAmount, sampleInstant);

        assertThat(deposit.getAmount()).isEqualTo(depositAmount);
        assertThat(deposit.getOperationType()).isEqualTo(OperationType.DEPOSIT);
        assertThat(deposit.getDateOfOperation()).isEqualTo(sampleInstant);
     }

    @Test
    @DisplayName("Should throw an InvalidAmountException when creating a deposit operation with a negative amount")
    void cannotCreateADepositWithNegativeAmount() {
        final Amount depositAmount = Amount.of(-40D);

        Throwable thrown = catchThrowable(() -> AccountOperation.createDepositOperation(depositAmount, sampleInstant));

        assertThat(thrown).isInstanceOf(InvalidAmountException.class);
    }

    @Test
    @DisplayName("Should be able to create a withdrawal operation given a negative amount")
    void canCreateAWithdrawWithNegativeAmount() throws InvalidAmountException {
        final Amount withdrawAmount = Amount.of(-40D);

        final AccountOperation withdrawal = AccountOperation.createWithdrawalOperation(withdrawAmount, sampleInstant);

        assertThat(withdrawal.getAmount()).isEqualTo(withdrawAmount);
        assertThat(withdrawal.getOperationType()).isEqualTo(OperationType.WITHDRAWAL);
        assertThat(withdrawal.getDateOfOperation()).isEqualTo(sampleInstant);
    }

    @Test
    @DisplayName("Should throw an InvalidAmountException when creating a withdraw operation with a positive amount")
    void cannotCreateAWithdrawWithPositiveAmount() {
        final Amount withdrawAmount = Amount.of(40D);

        Throwable thrown = catchThrowable(() -> AccountOperation.createWithdrawalOperation(withdrawAmount, sampleInstant));

        assertThat(thrown).isInstanceOf(InvalidAmountException.class);
    }

}