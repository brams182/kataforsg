package com.bbiki.sgkatabank.dao;

import com.bbiki.sgkatabank.domain.AccountOperation;
import com.bbiki.sgkatabank.domain.Amount;
import com.bbiki.sgkatabank.domain.exceptions.BankAccountNotFoundException;

import java.util.List;

public interface BankAccountRepository {
    void addOperation(String accountNumber, AccountOperation accountOperation) throws BankAccountNotFoundException;

    List<AccountOperation> getAccountOperations(String accountNumber) throws BankAccountNotFoundException;
}
