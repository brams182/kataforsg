package com.bbiki.sgkatabank.domain;

public enum OperationType {
    DEPOSIT, WITHDRAWAL
}
