package com.bbiki.sgkatabank.domain.exceptions;



public class BankAccountNotFoundException extends Exception {
    private static final String ERR_MSG_BEGINNING = "No bank account with number ";
    private static final String ERR_MSG_ENDING = " found";

    public BankAccountNotFoundException(String accountNumber) {
        super(ERR_MSG_BEGINNING + "[" + accountNumber + "]" + ERR_MSG_ENDING);
    }
}
