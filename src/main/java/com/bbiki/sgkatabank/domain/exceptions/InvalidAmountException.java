package com.bbiki.sgkatabank.domain.exceptions;

import com.bbiki.sgkatabank.domain.Amount;

import java.math.BigDecimal;

public class InvalidAmountException extends Exception {
    private static final String ERR_MSG_BEGINNING = " amount ";
    private static final String ERR_MSG_ENDING = " of the operation is not authorized";

    public InvalidAmountException(Amount amount) {
        super(ERR_MSG_BEGINNING + "[" + amount.asBigDecimal() + "]" + ERR_MSG_ENDING);
	}

}
