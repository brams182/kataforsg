package com.bbiki.sgkatabank.domain.exceptions;

import com.bbiki.sgkatabank.domain.Amount;

import java.math.BigDecimal;

public class InsufficientBalanceException extends Exception {
    private static final String ERR_MSG_BEGINNING = "insufficient  balance ! you can not withdraw amount ";
    private static final String ERR_MSG_ENDING = " from bank account : ";

    public InsufficientBalanceException(Amount amount, String accountNumber) {
        super(ERR_MSG_BEGINNING + "[" + amount.asBigDecimal() + "]" + ERR_MSG_ENDING + accountNumber);
    }
}
