package com.bbiki.sgkatabank.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Amount implements Comparable<Amount> {
    private static final int PRECISION = 12;
    private static final RoundingMode ROUND = RoundingMode.HALF_EVEN;
    private BigDecimal value;

    private Amount(BigDecimal value) {
        this.value = BigDecimal.valueOf(value.doubleValue()).setScale(PRECISION, ROUND);
    }

    public static Amount of(double amount) {
        return new Amount(BigDecimal.valueOf(amount));
    }

    public static Amount of(BigDecimal value) {
        return new Amount(value);
    }

    public boolean isPositive() {
        return value.compareTo(BigDecimal.ZERO) >= 0;
    }

    public boolean isNegative() {
        return !this.isPositive();
    }

    public int compareTo(Amount amountToCompare) {
        return this.value.compareTo(amountToCompare.asBigDecimal());
    }

    public BigDecimal asBigDecimal() {
        return this.value;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Amount)) {
            return false;
        }

        return this.value.equals(((Amount)obj).asBigDecimal());
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
