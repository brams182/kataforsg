package com.bbiki.sgkatabank.domain;

import com.bbiki.sgkatabank.domain.exceptions.InvalidAmountException;
import java.time.Instant;
import java.util.Objects;

public class AccountOperation {
    private final Amount amount;
    private final OperationType operationType;
    private final Instant dateOfOperation;

    private AccountOperation(Amount amount, OperationType operationType,
            Instant dateOfOperation) {
        this.amount = amount;
        this.operationType = operationType;
        this.dateOfOperation = dateOfOperation;
    }

    public Amount getAmount() {
        return amount;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public Instant getDateOfOperation() {
        return dateOfOperation;
    }

    public static AccountOperation createDepositOperation(Amount amount, Instant dateOfOperation) throws InvalidAmountException {
        if(amount.isNegative()) {
            throw new InvalidAmountException(amount);
        }

        return new AccountOperation(amount, OperationType.DEPOSIT, dateOfOperation);
    }

    public static AccountOperation createWithdrawalOperation(Amount amount, Instant dateOfOperation) throws InvalidAmountException {
        if(amount.isPositive()) {
            throw new InvalidAmountException(amount);
        }

        return new AccountOperation(amount, OperationType.WITHDRAWAL, dateOfOperation);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountOperation that = (AccountOperation) o;
        return Objects.equals(amount, that.amount) &&
                operationType == that.operationType &&
                Objects.equals(dateOfOperation, that.dateOfOperation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, operationType, dateOfOperation);
    }
}
