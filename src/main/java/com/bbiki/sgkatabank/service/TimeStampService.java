package com.bbiki.sgkatabank.service;

import java.time.Instant;

public interface TimeStampService {
    Instant utcNow();
}
