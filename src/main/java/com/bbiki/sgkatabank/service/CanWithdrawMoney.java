package com.bbiki.sgkatabank.service;

import com.bbiki.sgkatabank.domain.Amount;
import com.bbiki.sgkatabank.domain.exceptions.BankAccountNotFoundException;
import com.bbiki.sgkatabank.domain.exceptions.InsufficientBalanceException;
import com.bbiki.sgkatabank.domain.exceptions.InvalidAmountException;

public interface CanWithdrawMoney {
    void withdraw(String accountNumber, Amount amount) throws InvalidAmountException, BankAccountNotFoundException, InsufficientBalanceException;
}
