package com.bbiki.sgkatabank.service;

import java.math.BigDecimal;
import java.util.List;

import com.bbiki.sgkatabank.domain.AccountOperation;
import com.bbiki.sgkatabank.domain.Amount;
import com.bbiki.sgkatabank.domain.exceptions.BankAccountNotFoundException;

public interface StatementPrinterService {
    String printStatement(List<AccountOperation> operationsOfAccount, Amount bankAccountBalance);
}
