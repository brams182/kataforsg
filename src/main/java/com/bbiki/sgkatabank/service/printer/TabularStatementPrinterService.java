package com.bbiki.sgkatabank.service.printer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.bbiki.sgkatabank.domain.AccountOperation;
import com.bbiki.sgkatabank.domain.Amount;
import com.bbiki.sgkatabank.domain.OperationType;
import com.bbiki.sgkatabank.service.StatementPrinterService;

public class TabularStatementPrinterService implements StatementPrinterService {

    private static final String LINE_STOP = System.lineSeparator();
    private static final String DEBIT = "debit";
    private static final String CREDIT = "credit";
    private static final String SEPARATOR = "|";
    private static final String BALANCE = "Balance";
    private static final String DATE = "Date";
    private static final String EMPTY_LINE = "--------------------------------------------------------------------------------";
     private static final String STRING_FORMAT = "%20s %5s %15s %10s %15s %10s";

    @Override
    public String printStatement(List<AccountOperation> operationsOfAccount, Amount bankAccountBalance) {
        StringBuilder result = new StringBuilder();
        result.append(printHeader());

        for (AccountOperation operation : operationsOfAccount) {
            result.append(convertToPrintableItem(operation));
        }

        result.append(BALANCE + ": ");
        result.append(LINE_STOP);
        result.append(bankAccountBalance.asBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toPlainString());

        return result.toString();
    }

    private String printHeader() {
        StringBuilder result = new StringBuilder();

        result.append(String.format(STRING_FORMAT, DATE, SEPARATOR, CREDIT, SEPARATOR, DEBIT, SEPARATOR));
        result.append(LINE_STOP);
        result.append(String.format("%s", EMPTY_LINE));
        result.append(LINE_STOP);

        return result.toString();
    }

    private String convertToPrintableItem(AccountOperation operation) {
        String credit;
        String debit;
        if (operation.getOperationType().equals(OperationType.WITHDRAWAL)) {
            credit = "";
            debit = operation.getAmount().asBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toPlainString();
        }else {
            credit = operation.getAmount().asBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toPlainString();
            debit = "";
        }

        final String dateOfOperationPrinted = getDateOfOperationPrinted(operation.getDateOfOperation());

        final String format = String.format(STRING_FORMAT, dateOfOperationPrinted, SEPARATOR, credit, SEPARATOR, debit, SEPARATOR);

        return format + LINE_STOP;
    }

    private String getDateOfOperationPrinted(Instant dateOfOperation) {
        return DateTimeFormatter.ISO_INSTANT.format(dateOfOperation);
    }
}
