package com.bbiki.sgkatabank.service;

import com.bbiki.sgkatabank.domain.Amount;
import com.bbiki.sgkatabank.domain.exceptions.BankAccountNotFoundException;
import com.bbiki.sgkatabank.domain.exceptions.InvalidAmountException;

public interface CanDepositMoney {
    void deposit(String accountNumber, Amount amount) throws InvalidAmountException, BankAccountNotFoundException;
}
