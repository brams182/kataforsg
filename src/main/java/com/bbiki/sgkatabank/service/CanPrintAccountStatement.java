package com.bbiki.sgkatabank.service;

import com.bbiki.sgkatabank.domain.exceptions.BankAccountNotFoundException;

public interface CanPrintAccountStatement {
    String print(String accountNumber, StatementPrinterService statementPrinterService) throws BankAccountNotFoundException;
}
