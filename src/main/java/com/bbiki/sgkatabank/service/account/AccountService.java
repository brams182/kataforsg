package com.bbiki.sgkatabank.service.account;

import com.bbiki.sgkatabank.dao.BankAccountRepository;
import com.bbiki.sgkatabank.domain.*;
import com.bbiki.sgkatabank.domain.exceptions.BankAccountNotFoundException;
import com.bbiki.sgkatabank.domain.exceptions.InsufficientBalanceException;
import com.bbiki.sgkatabank.domain.exceptions.InvalidAmountException;
import com.bbiki.sgkatabank.service.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class AccountService implements CanDepositMoney, CanWithdrawMoney, CanPrintAccountStatement {
    private final BankAccountRepository bankAccountRepository;
    private final TimeStampService timeService;

    public AccountService(BankAccountRepository bankAccountRepository, TimeStampService timeService) {
        this.bankAccountRepository = bankAccountRepository;
        this.timeService = timeService;
    }

    @Override
    public void deposit(String accountNumber, Amount depositAmount) throws InvalidAmountException, BankAccountNotFoundException {
        AccountOperation depositOperation = AccountOperation.createDepositOperation(depositAmount, timeService.utcNow());

        this.bankAccountRepository.addOperation(accountNumber, depositOperation);
    }
    @Override
    public void withdraw(String accountNumber, Amount withdrawalAmount)
            throws InvalidAmountException, BankAccountNotFoundException, InsufficientBalanceException {
        AccountOperation withdrawOperation = AccountOperation.createWithdrawalOperation(withdrawalAmount, timeService.utcNow());

        final Amount balance = this.computeBalance(accountNumber);

        final BigDecimal newBalance = balance.asBigDecimal().add(withdrawalAmount.asBigDecimal());

        if (newBalance.compareTo(BigDecimal.ZERO) < 0) {
            throw new InsufficientBalanceException(withdrawalAmount, accountNumber);
        }

        this.bankAccountRepository.addOperation(accountNumber, withdrawOperation);
    }

    @Override
    public String print(String accountNumber, StatementPrinterService statementPrinterService) throws BankAccountNotFoundException {
        final Amount balance = this.computeBalance(accountNumber);
        final List<AccountOperation> accountOperations = this.bankAccountRepository.getAccountOperations(accountNumber);

        return statementPrinterService.printStatement(accountOperations, balance);
    }

    private Amount computeBalance(String accountNumber) throws BankAccountNotFoundException {
        final List<AccountOperation> accountOperations = this.bankAccountRepository.getAccountOperations(accountNumber);

        BigDecimal balance = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_EVEN);

        for(AccountOperation operation : accountOperations) {
            balance=balance.add(operation.getAmount().asBigDecimal());
        }

        return Amount.of(balance);
    }
}
